import MyButton from '../components/Button.vue';

export default {
  title: 'Example/Button',
  component: MyButton,
  argTypes: {
    btnType: { control: { type: 'select', options: ['primary', 'secondary', 'alert'] }, defaultValue: 'primary'},
  },
};

const Template = (args, { argTypes }) => {
  const {btnType} = args;
  // HERE: this is the idea found in: https://storybook.js.org/docs/vue/essentials/controls#dealing-with-complex-values
  args.primary = btnType === "primary";
  args.secondary = btnType === "secondary";
  args.alert = btnType === "alert";
  return {
    props: Object.keys(argTypes),
    components: { MyButton },
    template: '<my-button v-bind="$props" />',
  };
};

export const Standard = Template.bind({});

export const Primary = Template.bind({});
Primary.args = {
  primary: true,
};
Primary.parameters = {
  docs: {
    description: {
      story: "The story with preselected **primary** as type",
    }
  }
}

export const Secondary = Template.bind({});
Secondary.args = {
  secondary: true,
};
Secondary.parameters = {
  docs: {
    description: {
      story: "The story with preselected **secondary** as type",
    }
  }
}

export const Alert = Template.bind({});
Alert.args = {
  alert: true,
};
Alert.parameters = {
  docs: {
    description: {
      story: "The story with preselected **alert** as type",
    }
  }
}
